#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

typedef struct {
	int port;
	bool plug;
} config_t;

config_t* config_malloc() {
	return (config_t*)malloc(sizeof(config_t));
}

void config_ctor(config_t* config) {
	const char* port_from_env = getenv("MY_PORT");
	if (port_from_env == NULL) {
		config->port = 2626;
	} else {
		config->port = atoi(port_from_env);
	}

	printf("%d\n", config->port);
	
	config->plug = true;
}

void config_dtor(config_t* config) {
	config->port = 0;
}

int config_get_port(config_t* config) {
	return config->port;
}

bool config_get_plug(config_t* config) {
	return config->plug;
}
